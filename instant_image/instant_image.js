pending_search = 0;
flickrLicenseIDs = {
  //0 : 'all-reserved',
  1 : ['BY', 'NC', 'SA'],
  2 : ['BY', 'NC'],
  3 : ['BY', 'NC', 'ND'],
  4 : ['BY'],
  5 : ['BY', 'SA'],
  6 : ['BY', 'ND'],
  7 : ['PD'],
  8 : ['PD', 'US-gov']
};

/**
 * Function : dump()
 * Arguments: The data - array,hash(associative array),object
 *    The level - OPTIONAL
 * Returns  : The textual representation of the array.
 * This function was inspired by the print_r function of PHP.
 * This will accept some data as the argument and return a
 * text that will be a more readable version of the
 * array/hash/object that is given.
 * Docs: http://www.openjs.com/scripts/others/dump_function_php_print_r.php
 */
function dump(arr, level) {
  var dumped_text = "";
  if(!level)
    level = 0;

  //The padding given at the beginning of the line.
  var level_padding = "";
  for(var j = 0; j < level + 1; j++)
  level_padding += "    ";

  if( typeof (arr) == 'object') {//Array/Hashes/Objects
    for(var item in arr) {
      var value = arr[item];

      if( typeof (value) == 'object') {//If it is an array,
        dumped_text += level_padding + "'" + item + "' ...\n";
        dumped_text += dump(value, level + 1);
      } else {
        dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
      }
    }
  } else {//Stings/Chars/Numbers etc.
    dumped_text = "===>" + arr + "<===(" + typeof (arr) + ")";
  }
  return dumped_text;
}


/*
 * Carry out an image search (js only - deprecated)
 * */
function doSearch() {
  var suitableLicenses = [1, 2, 3, 4, 5, 6, 7, 8]; //[4, 7, 8];

  if($('license-copyleft').attr('checked') == true) {
    suitableLicenses.push(2);
  };

  $('#search-busy').css('visibility', 'visible');
  $.ajax({
    url : "/services/search.php",
    //context : document.body,
    data : {
      'text' : $('#search-string').val(),
      'license' : suitableLicenses.join(','),
      'per_page' : 10
    },
    type : 'GET',
    dataType : 'json',
    cache : false, //DEBUG
    success : function(data) {
      pending_search = 0;
      $('#results').html('');
      for(var i = 0; i < data.photos.photo.length; i++) {
        $('#results').append($('<div class="flickr-image-wrapper" rel="' + data.photos.photo[i]["id"] + '"><img src="http://farm' + data.photos.photo[i]["farm"] + '.static.flickr.com/' + data.photos.photo[i]["server"] + '/' + data.photos.photo[i]["id"] + '_' + data.photos.photo[i]["secret"] + '_m.jpg" alt="' + data.photos.photo[i]["title"] + '" /><div class="attribution"><span class="title">' + data.photos.photo[i]["title"] + '</span></div></div>'));
      };
      $('#search-busy').css('visibility', 'hidden');

      $('#data-dump').html(dump(data));
    }
  });
};

/*
 * Insert metadata onto an image
 */
function insertMetadata(photoId) {
  jQuery.ajax({
    url : "/instant_image/ajax/info",
    //context : document.body,
    data : {
      'photo_id' : photoId
    },
    type : 'GET',
    dataType : 'json',
    cache : false, //DEBUG
   
    success : function(data) {
      if(data.code == 1) {
        jQuery('#results').html('&#9888;' + Drupal.t(' Flickr metadata lookup failed.<br/>' + dump(data)));
      } else {
        var attributionBlock = jQuery('[rel="' + data.photo.id + '"]').find('.attribution');

        var realName = '';
        if(data.photo.owner.realname != '') {
          realName = ' <span class="real-name">(' + data.photo.owner.realname + ')</span> ';
        }
        attributionBlock.append('<span class="author"><a class="source-link" href="' + data.photo.urls.url[0]._content + '">' + data.photo.owner.username + '</a>' + realName + '</span>');

        var licenseBlock = attributionBlock.find('.license');
        for(var i = flickrLicenseIDs[data.photo.license].length - 1; i >= 0; i--) {
          var code = flickrLicenseIDs[data.photo.license][i];
          var licenseIcon = jQuery('<div class="icon license-' + code + '"></div>');
          licenseBlock.append(licenseIcon);
        };
      }
    }
  });
}


( function($) {
  Drupal.behaviors.instantImageInit = {
    attach : function(context, settings) {
        $('.flickr-image-wrapper').live('hover', function() {
          //check if metadata has been loaded
          if($(this).find('.license').length == 0) {
            //add license block to avoid loading twice
            $(this).find('.attribution').prepend('<span class="license"></span>');

            insertMetadata($(this).attr('rel'));
          }
        });
    }
  }
}(jQuery));

