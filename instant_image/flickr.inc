<?php 

class Flickr {
  private $apiKey = '';
  public $flickr_rest = 'http://flickr.com/services/rest/';
  private $flickr_standard_options;
  private $flickr_return_formats;

  public function __construct() {
    $this-> flickr_return_formats = array ('json' => "&nojsoncallback=1&format=json",'php' => '&format=php_serial');
    $this -> flickr_standard_options = '&api_key=' . $this -> apiKey;
  }

  public function prepare_args($method,$request_args,$format='php',$options='') {
    $output = $this -> flickr_rest . '?method=' . $method . $this -> flickr_standard_options . $this->flickr_return_formats[$format] . $options;
    
    foreach ($request_args as $key => $value) {
      $output .= '&' . stripslashes(urlencode($key)) . '=' . stripslashes(urlencode($value));
    }
    return $output;
  }

  public function search($request_args) {
    return unserialize(file_get_contents($this-> prepare_args('flickr.photos.search',$request_args,'php','&media=photos')));
  }

  public function getInfo($request_args,$format) {
    return file_get_contents($this-> prepare_args('flickr.photos.getInfo',$request_args,$format));
  }
}

?>