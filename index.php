<!DOCTYPE HTML>
<html>
  <head>
    <title>Instant image mockup</title>
    <link rel="stylesheet" type="text/css" href="jquery-ui/css/flick/jquery-ui-1.8.16.custom.css" />
    <script type="text/javascript" src="jquery-ui/js/jquery-1.6.2.min.js" ></script>
    <script type="text/javascript" src="jquery-ui/js/jquery-ui-1.8.16.custom.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="instant_image.css" />
    <script type="text/javascript" src="instant_image.js" ></script>
  </head>
  <body>
    <div id="search-wrapper">
      <input type="search" id ="search-string" placeholder="Image search" />
      <!-- <input type="button" id ="search-button" value="search"/> -->
      <div id="search-busy" class="ui-icon ui-icon-search"></div>
      <!--<div id="settings-wrapper">
        <button id="license-free">
          All free
        </button>
        <button id="license-copyleft">
          Copyleft
        </button>
        <button id="license-nc">
          Noncommercial
        </button>
      </div> -->
    </div>
    <div id="results"></div>
    <div id="data-dump"></div>
  </body>
</html>